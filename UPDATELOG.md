# Update Log



##### KeraDEX V.0.1
- Limit order trading on DEX

##### KeraDEX V.0.2
- Connect with Phantom and Sollet.io wallet
- Add token symbol for each market list
- Rearrange market list
- Show market list on dropdown trade menu
- Add wallet icon to show status on connect wallet button
- Update UI : Font, colors and button style

##### KeraDEX V.0.2.1
- Connect with Solflare wallet

##### KeraDEX V.0.3
- Add swap, farms, pools, add liquidity

##### KeraDEX V.0.3.1
- Change endpoint
